package sourceinformation.com.br.teste.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sourceinformation.com.br.teste.R;

public class CalculadoraActivity extends AppCompatActivity {

    private EditText edtGasolina;
    private EditText edtEtanol;
    private TextView txtValorCalculado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        edtGasolina = (EditText) findViewById(R.id.edtGasolina);
        edtEtanol = (EditText) findViewById(R.id.edtEtanol);
        txtValorCalculado = (TextView) findViewById(R.id.txtValorCalculado);

    }

    public void clickCalcular(View view) {
        txtValorCalculado.setText(this.calcularMelhorOpcao(view));
    }

    private String calcularMelhorOpcao(View view) {
        double valorGasolina = Double.parseDouble(edtGasolina.getText().toString());
        double valorEtanol = Double.parseDouble(edtEtanol.getText().toString());

        String resultado = "A melhor opção é: ";

        if ((valorGasolina * 0.7) > valorEtanol) {
            resultado += "Etanol";
        } else {
            resultado += "Gasolina";
        }

        return resultado;
    }
}
